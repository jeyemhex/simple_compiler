/*======[ EMULATOR ]======
 *
 * Author:  Edward Higgins <ed.j.higgins@gmail.com>
 *
 * Version: 0.1.1, 2017-03-19
 *
 * This code is distributed under the MIT license.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "asm.h"

#define MEM_SIZE    256
#define BUFF_SIZE   24

struct Machine {
    // Stack memory
    uint16_t *memory;

    // General purpose registers
    uint16_t registers[4];

    // Instruction & Stack pointers
    uint16_t IP;
    uint16_t SP;

    // Zero, Carry and  Failure flags
    bool ZF;
    bool CF;
    bool FF;

    char *std_out;
};

struct Machine *new_machine() {

    struct Machine *m = malloc(sizeof(struct Machine));

    for(int i=0; i<4; i++) {
        m->registers[i] = 0x0;
    }

    m->IP = 0x0;
    m->SP = MEM_SIZE - BUFF_SIZE;

    m->ZF = false;
    m->CF = false;
    m->FF = false;

    m->memory = malloc(sizeof(uint16_t) * MEM_SIZE);
    for(int i=0; i<MEM_SIZE; i++) {
        m->memory[i] = 0x0;
    }

    m->std_out = malloc(BUFF_SIZE * sizeof(char));

    for(int i=0; i<BUFF_SIZE; i++) {
        m->std_out[i] = 0x0;
    }

    return m;
}

void memory_dump(uint16_t *mem) {
    int row = 0;
    printf("\nMEMORY:\n");
    printf("      |  *0   *1   *2   *3   *4   *5   *6   *7   *8   *9   *a   *b   *c   *d   *e   *f\n");
    printf("------+-------------------------------------------------------------------------------\n%4x* |", row++);
    for (int i=0; i<MEM_SIZE; i++) {
        printf("%04x ", mem[i]);
        if(i%16 == 15 && (i+1 < MEM_SIZE)) printf("\n%4x* |", row++);
    }
    printf("\n");
}

void machine_dump(struct Machine *m) {
    printf("\nMachine dumped!\n---------------\n");
    printf("\nREGISTERS:\n");
    printf("  A  B  C  D  IP SP ZF CF FF\n");
    printf("  %02x %02x %02x %02x %02x %02x %d  %d  %d\n",
            m->registers[0], m->registers[1], m->registers[2], m->registers[3],
            m->IP, m->SP, m->ZF, m->CF, m->FF);

    memory_dump(m->memory);
}

void emulate(struct Machine *m) {
    // Do while we haven't been told to halt:
    while (m->memory[m->IP] != HLT) {

        //printf("0x%02x ",m->memory[m->IP]);
        // What have we been told to do?
        switch(m->memory[m->IP]) {
            case MOV_REG_REG:
                m->registers[m->memory[m->IP+1]] = m->registers[m->memory[m->IP+2]];
                m->IP += 3;
                break;

            case MOV_REG_ADDR:
                m->registers[m->memory[m->IP+1]] = m->memory[m->memory[m->IP+2]];
                m->IP += 3;
                break;

            case MOV_REG_RADDR:
                m->registers[m->memory[m->IP+1]] = m->memory[m->registers[m->memory[m->IP+2]]];
                m->IP += 3;
                break;

            case MOV_ADDR_REG:
                m->memory[m->memory[m->memory[m->IP+1]]] = m->registers[m->memory[m->IP+2]];
                m->IP += 3;
                break;

            case MOV_RADDR_REG:
                m->memory[m->registers[m->memory[m->IP+1]]] = m->registers[m->memory[m->IP+2]];
                m->IP += 3;
                break;

            case MOV_REG_CONST:
                m->registers[m->memory[m->IP+1]] = m->memory[m->IP+2];
                m->IP += 3;
                break;

            case MOV_ADDR_CONST:
                m->memory[m->memory[m->memory[m->IP+1]]] = m->memory[m->IP+2];
                m->IP += 3;
                break;

            case MOV_RADDR_CONST:
                m->memory[m->registers[m->memory[m->IP+1]]] = m->memory[m->IP+2];
                m->IP += 3;
                break;

            case ADD_REG_REG:
                m->registers[m->memory[m->IP+1]] += m->registers[m->memory[m->IP+2]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case ADD_REG_RADDR:
                m->registers[m->memory[m->IP+1]] += m->memory[m->registers[m->memory[m->IP+2]]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case ADD_REG_ADDR:
                m->registers[m->memory[m->IP+1]] += m->memory[m->memory[m->IP+2]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case ADD_REG_CONST:
                m->registers[m->memory[m->IP+1]] += m->memory[m->IP+2];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case SUB_REG_REG:
                m->registers[m->memory[m->IP+1]] -= m->registers[m->memory[m->IP+2]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case SUB_REG_RADDR:
                m->registers[m->memory[m->IP+1]] -= m->memory[m->registers[m->memory[m->IP+2]]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case SUB_REG_ADDR:
                m->registers[m->memory[m->IP+1]] -= m->memory[m->memory[m->IP+2]];
                m->IP += 3;

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                break;

            case SUB_REG_CONST:
                m->registers[m->memory[m->IP+1]] -= m->memory[m->IP+2];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case INC_REG:
                ++m->registers[m->memory[m->IP+1]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 2;
                break;

            case DEC_REG:
                --m->registers[m->memory[m->IP+1]];
                m->IP += 2;

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                break;

            case CMP_REG_REG:
                if (m->registers[m->memory[m->IP+1]] == m->registers[m->memory[m->IP+2]]) {
                    m->ZF = 1;
                }
                m->IP += 3;
                break;

            case CMP_REG_RADDR:
                if (m->registers[m->memory[m->IP+1]] == m->memory[m->registers[m->memory[m->IP+2]]]) {
                    m->ZF = 1;
                }
                m->IP += 3;
                break;

            case CMP_REG_ADDR:
                if (m->registers[m->memory[m->IP+1]] == m->memory[m->memory[m->IP+2]]) {
                    m->ZF = 1;
                }
                m->IP += 3;
                break;

            case CMP_REG_CONST:
                if (m->registers[m->memory[m->IP+1]] == m->memory[m->IP+2]) {
                    m->ZF = 1;
                }
                m->IP += 3;
                break;

            case JMP_RADDR:
                m->IP = m->memory[m->IP+1];
                break;

            case JMP_ADDR:
                m->IP = m->memory[m->IP+1];
                break;

            case JC_RADDR:
                if ( m->CF ) {
                    m->IP = m->memory[m->IP+1];
                } else {
                    m->IP += 2;
                }
                break;

            case JC_ADDR:
                if ( m->CF ) {
                    m->IP = m->memory[m->IP+1];
                } else {
                    m->IP += 2;
                }
                break;

            case JNC_RADDR:
                if ( ! m->CF ) {
                    m->IP = m->memory[m->IP+1];
                } else {
                    m->IP += 2;
                }
                break;

            case JNC_ADDR:
                if ( ! m->CF ) {
                    m->IP = m->memory[m->IP+1];
                } else {
                    m->IP += 2;
                }
                break;

            case JZ_RADDR:
                if ( m->ZF ) {
                    m->IP = m->memory[m->IP+1];
                } else {
                    m->IP += 2;
                }
                break;

            case JZ_ADDR:
                if ( m->ZF ) {
                    m->IP = m->memory[m->IP+1];
                } else {
                    m->IP += 2;
                }
                break;

            case JNZ_RADDR:
                if ( !m->ZF ) {
                    m->IP = m->memory[m->IP+1];
                } else {
                    m->IP += 2;
                }
                break;

            case JNZ_ADDR:
                if ( !m->ZF ) {
                    m->IP = m->memory[m->IP+1];
                } else {
                    m->IP += 2;
                }
                break;

            case JA_RADDR:
                if ( !m->CF && !m->ZF ) {
                    m->IP = m->memory[m->IP+1];
                } else {
                    m->IP += 2;
                }
                break;

            case JA_ADDR:
                if ( !m->CF && !m->ZF ) {
                    m->IP = m->memory[m->IP+1];
                } else {
                    m->IP += 2;
                }
                break;

            case JNA_RADDR:
                if ( m->CF || m->ZF ) {
                    m->IP = m->memory[m->IP+1];
                } else {
                    m->IP += 2;
                }
                break;

            case JNA_ADDR:
                if ( m->CF || m->ZF ) {
                    m->IP = m->memory[m->IP+1];
                } else {
                    m->IP += 2;
                }
                break;

            case PUSH_REG:
                --m->SP;
                m->memory[m->SP] = m->registers[m->memory[m->IP+1]];
                m->IP += 2;
                break;

            case PUSH_RADDR:
                --m->SP;
                m->memory[m->SP] = m->memory[m->registers[m->memory[m->IP+1]]];
                m->IP += 2;
                break;

            case PUSH_ADDR:
                --m->SP;
                m->memory[m->SP] = m->memory[m->memory[m->IP+1]];
                m->IP += 2;
                break;

            case PUSH_CONST:
                --m->SP;
                m->memory[m->SP] = m->memory[m->IP+1];
                m->IP += 2;
                break;

            case POP_REG:
                m->registers[m->memory[m->IP+1]] = m->memory[m->SP];
                ++m->SP;
                m->IP += 2;
                break;

            case CALL_RADDR:
                --m->SP;
                m->memory[m->SP] = m->IP+2;
                m->IP = m->memory[m->IP+1];
                break;

            case CALL_ADDR:
                --m->SP;
                m->memory[m->SP] = m->IP+2;
                m->IP = m->memory[m->IP+1];
                break;

            case RET:
                m->IP = m->memory[m->SP];
                ++m->SP;
                break;

            case MUL_REG:
                m->registers[0] *= m->registers[m->memory[m->IP+1]];

                if(m->registers[0] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 2;
                break;

            case MUL_RADDR:
                m->registers[0] *= m->memory[m->registers[m->memory[m->IP+1]]];

                if(m->registers[0] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 2;
                break;

            case MUL_ADDR:
                m->registers[0] *= m->memory[m->memory[m->IP+1]];

                if(m->registers[0] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 2;
                break;

            case MUL_CONST:
                m->registers[0] *= m->memory[m->IP+1];

                if(m->registers[0] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 2;
                break;

            case DIV_REG:
                m->registers[0] /= m->registers[m->memory[m->IP+1]];

                if(m->registers[0] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 2;
                break;

            case DIV_RADDR:
                m->registers[0] /= m->memory[m->registers[m->memory[m->IP+1]]];

                if(m->registers[0] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 2;
                break;

            case DIV_ADDR:
                m->registers[0] /= m->memory[m->memory[m->IP+1]];

                if(m->registers[0] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 2;
                break;

            case DIV_CONST:
                m->registers[0] /= m->memory[m->IP+1];

                if(m->registers[0] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 2;
                break;

            case AND_REG_REG:
                m->registers[m->memory[m->IP+1]] &= m->registers[m->memory[m->IP+2]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case AND_REG_RADDR:
                m->registers[m->memory[m->IP+1]] &= m->memory[m->registers[m->memory[m->IP+2]]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case AND_REG_ADDR:
                m->registers[m->memory[m->IP+1]] &= m->memory[m->memory[m->IP+2]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case AND_REG_CONST:
                m->registers[m->memory[m->IP+1]] &= m->memory[m->IP+2];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case OR_REG_REG:
                m->registers[m->memory[m->IP+1]] |= m->registers[m->memory[m->IP+2]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case OR_REG_RADDR:
                m->registers[m->memory[m->IP+1]] |= m->memory[m->registers[m->memory[m->IP+2]]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case OR_REG_ADDR:
                m->registers[m->memory[m->IP+1]] |= m->memory[m->memory[m->IP+2]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case OR_REG_CONST:
                m->registers[m->memory[m->IP+1]] |= m->memory[m->IP+2];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case XOR_REG_REG:
                m->registers[m->memory[m->IP+1]] ^= m->registers[m->memory[m->IP+2]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case XOR_REG_RADDR:
                m->registers[m->memory[m->IP+1]] ^= m->memory[m->registers[m->memory[m->IP+2]]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case XOR_REG_ADDR:
                m->registers[m->memory[m->IP+1]] ^= m->memory[m->memory[m->IP+2]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case XOR_REG_CONST:
                m->registers[m->memory[m->IP+1]] ^= m->memory[m->IP+2];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case NOT_REG :
                m->registers[m->memory[m->IP+1]] = ! m->registers[m->memory[m->IP+1]];
                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 2;
                break;

            case SHL_REG_REG:
                m->registers[m->memory[m->IP+1]] <<= m->registers[m->memory[m->IP+2]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case SHL_REG_RADDR:
                m->registers[m->memory[m->IP+1]] <<= m->memory[m->registers[m->memory[m->IP+2]]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case SHL_REG_ADDR:
                m->registers[m->memory[m->IP+1]] <<= m->memory[m->memory[m->IP+2]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case SHL_REG_CONST:
                m->registers[m->memory[m->IP+1]] <<= m->memory[m->IP+2];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case SHR_REG_REG:
                m->registers[m->memory[m->IP+1]] >>= m->registers[m->memory[m->IP+2]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case SHR_REG_RADDR:
                m->registers[m->memory[m->IP+1]] >>= m->memory[m->registers[m->memory[m->IP+2]]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case SHR_REG_ADDR:
                m->registers[m->memory[m->IP+1]] >>= m->memory[m->memory[m->IP+2]];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;

            case SHR_REG_CONST:
                m->registers[m->memory[m->IP+1]] >>= m->memory[m->IP+2];

                if(m->registers[m->memory[m->IP+1]] == 0) {
                    m->ZF = 1;
                } else {
                    m->ZF = 0;
                }

                m->IP += 3;
                break;


            default:
                fprintf(stderr,"Invalid command: 0x%02x at 0x%02x\n", m->memory[m->IP], m->IP);
                machine_dump(m);
                exit(-1);
        }
    }
    for(int i=0; i<BUFF_SIZE; i++) {
        m->std_out[i] = m->memory[i+MEM_SIZE-BUFF_SIZE];
    }
}

int main(int argc, char *argv[]) {

    struct Machine *m = new_machine();

    // Open up the file argv[1] and copy its contents into our machine's memory
    FILE *bin_file = fopen(argv[1], "rb");

    for (int i=0;i<MEM_SIZE; i++){
        if (feof(bin_file)) break;
        m->memory[i] = getc(bin_file);
    }

    fclose(bin_file);

//    machine_dump(m);
    emulate(m);

    printf("%s\n", m->std_out);
    machine_dump(m);

    return 0;
}
