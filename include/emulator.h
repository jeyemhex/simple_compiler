/*
 * emulator.h
 * Copyright (C) 2017 Edward Higgins <ed.j.higgins@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef EMULATOR_H
#define EMULATOR_H

#define STACK_SIZE 1024

#endif /* !EMULATOR_H */
