/*
 * asm.h
 * Copyright (C) 2017 Edward Higgins <ed.j.higgins@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef ASM_H
#define ASM_H

#define HLT             0x00
#define MOV_REG_REG     0x01
#define MOV_REG_ADDR    0x02
#define MOV_REG_RADDR   0x03
#define MOV_ADDR_REG    0x04
#define MOV_RADDR_REG   0x05
#define MOV_REG_CONST   0x06
#define MOV_ADDR_CONST  0x07
#define MOV_RADDR_CONST 0x08
#define ADD_REG_REG     0x0a
#define ADD_REG_RADDR   0x0b
#define ADD_REG_ADDR    0x0c
#define ADD_REG_CONST   0x0d
#define SUB_REG_REG     0x0e
#define SUB_REG_RADDR   0x0f
#define SUB_REG_ADDR    0x10
#define SUB_REG_CONST   0x11
#define INC_REG         0x12
#define DEC_REG         0x13
#define CMP_REG_REG     0x14
#define CMP_REG_RADDR   0x15
#define CMP_REG_ADDR    0x16
#define CMP_REG_CONST   0x17
#define JMP_RADDR       0x1e
#define JMP_ADDR        0x1f
#define JC_RADDR        0x20
#define JC_ADDR         0x21
#define JNC_RADDR       0x22
#define JNC_ADDR        0x23
#define JZ_RADDR        0x24
#define JZ_ADDR         0x25
#define JNZ_RADDR       0x26
#define JNZ_ADDR        0x27
#define JA_RADDR        0x28
#define JA_ADDR         0x29
#define JNA_RADDR       0x2a
#define JNA_ADDR        0x2b
#define PUSH_REG        0x32
#define PUSH_RADDR      0x33
#define PUSH_ADDR       0x34
#define PUSH_CONST      0x35
#define POP_REG         0x36
#define CALL_RADDR      0x37
#define CALL_ADDR       0x38
#define RET             0x39
#define MUL_REG         0x3c
#define MUL_RADDR       0x3d
#define MUL_ADDR        0x3e
#define MUL_CONST       0x3f
#define DIV_REG         0x40
#define DIV_RADDR       0x41
#define DIV_ADDR        0x42
#define DIV_CONST       0x43
#define AND_REG_REG     0x46
#define AND_REG_RADDR   0x47
#define AND_REG_ADDR    0x48
#define AND_REG_CONST   0x49
#define OR_REG_REG      0x4a
#define OR_REG_RADDR    0x4b
#define OR_REG_ADDR     0x4c
#define OR_REG_CONST    0x4d
#define XOR_REG_REG     0x4e
#define XOR_REG_RADDR   0x4f
#define XOR_REG_ADDR    0x50
#define XOR_REG_CONST   0x51
#define NOT_REG         0x52
#define SHL_REG_REG     0x5a
#define SHL_REG_RADDR   0x5b
#define SHL_REG_ADDR    0x5c
#define SHL_REG_CONST   0x5d
#define SHR_REG_REG     0x5e
#define SHR_REG_RADDR   0x5f
#define SHR_REG_ADDR    0x60
#define SHR_REG_CONST   0x61


#endif /* !ASM_H */
