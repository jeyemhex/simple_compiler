CC=gcc

vpath %.c src/
vpath %.h include/

.PHONY: all clean

all: emulator

emulator: emulator.c

%: %.c 
	$(CC) $< -o bin/$@ -Iinclude
clean:
	rm -f bin/*

